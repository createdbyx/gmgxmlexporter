﻿namespace Codefarts.GMGXmlExporter
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Text;
    using System.Xml;
    using System.Xml.Serialization;

    using Codefarts.CoreProjectCode.Settings;
    using Codefarts.GeneralTools.Models;
    using Codefarts.GridMapGame;
    using Codefarts.GridMapGame.Code;
    using Codefarts.GridMapGame.GameScreens;
    using Codefarts.GridMapGame.MapEditor;
    using Codefarts.GridMapGame.Models;
    using Codefarts.GridMapGame.Scripts;
    using Codefarts.MeshMaps.Scripts;
    using Codefarts.UnityThreading;
    using Codefarts.XnaGameScreens;

    using UnityEngine;

    public class XmlMapExporter : IMapExporter<MapEditorScreen>
    {
        private PluginInformation pluginInformation;

        private MapEditorScreen editor;
        private ProgressArgs args = new ProgressArgs();
        private BrowseForFileScreen browseForFileScreen;

        private Texture2D mapFileIcon;

        private bool isCanceled;

        private bool isExporting;

        private bool isProcessing;

        /// <summary>
        /// Initializes a new instance of the <see cref="XmlMapExporter"/> class.
        /// </summary>
        public XmlMapExporter()
        {
            this.pluginInformation = new PluginInformation()
            {
                Title = "XML Map Exporter",
                Uid = new Guid("3D1E3A1E-E1D8-42DE-A4DA-E7C994191BB6"),
            };
        }

        /// <summary>
        /// Gets the information for the plugin.
        /// </summary>   
        public PluginInformation Information
        {
            get
            {
                return new PluginInformation(this.pluginInformation);
            }
        }

        public void Shutdown()
        {
            this.editor = null;
        }

        public void Startup(MapEditorScreen editor)
        {
            this.editor = editor;

            var settings = GameObject.FindObjectOfType<SettingsMonoBehaviour>();
            var settingValue = settings.Settings.GetSetting(EditorSettingKeys.MapIcon, SettingKeys.NoIconTexture);
            this.mapFileIcon = Resources.Load<Texture2D>(settingValue);

            this.browseForFileScreen = new BrowseForFileScreen();
            this.browseForFileScreen.Path = Application.dataPath;
            settingValue = settings.Settings.GetSetting(SettingKeys.FolderIcon, SettingKeys.NoIconTexture);
            this.browseForFileScreen.FolderIcon = Resources.Load<Texture2D>(settingValue);
            settingValue = settings.Settings.GetSetting(SettingKeys.OpenFolderIcon, SettingKeys.NoIconTexture);
            this.browseForFileScreen.ParentFolderIcon = Resources.Load<Texture2D>(settingValue);
            this.browseForFileScreen.Accepted = this.FileAccepted;
            this.browseForFileScreen.Canceled = this.FileCanceled;
            this.browseForFileScreen.IconGenerator = path => this.mapFileIcon;
            this.browseForFileScreen.Extensions = new[] { this.Extension };
        }

        private void FileCanceled()
        {
            this.isExporting = false;
            this.OnCanceledProgress();
        }

        private struct ItemContainer
        {
            public Point2D Location;
            public object[] Stack;
        }

        private void FileAccepted(string path)
        {
#if UNITY_WEBPLAYER
            GuiMessageBoxScreen.Show(this.editor.ScreenManager, "Not Implemented for web player", new[] { "OK" }, 0);
#else
         if (this.isCanceled)
            {
                this.OnCanceledProgress();
            }

            var mapObject = this.editor.Map.CurrentMap;
            if (mapObject == null)
            {
                this.isExporting = false;
                this.OnProgress(new NullReferenceException("There is no map available."));
                return;
            }

            var map = mapObject.GetComponent<GridMapManagerBase>();
            if (map == null)
            {
                this.isExporting = false;
                this.OnProgress(new NullReferenceException("Map has no map manager."));
                return;
            }

            this.OnProgress();

            Threading.QueueTask(t =>
                {
                    this.isProcessing = true;
                    try
                    {
                        // var dotCount = 0;
                        //  var lastUpdate = DateTime.Now;
                        var tempFile = Path.GetTempFileName();
                        if (File.Exists(tempFile))
                        {
                            File.Delete(tempFile);
                        }

                        using (var stream = new FileStream(tempFile, FileMode.CreateNew, FileAccess.Write))
                        {
                            var settings = new XmlWriterSettings();
                            settings.Indent = true;
                            settings.IndentChars = " ";
                            settings.Encoding = Encoding.UTF8;

                            using (var writer = XmlWriter.Create(stream, settings))
                            {
                                writer.WriteStartDocument(true);
                                writer.WriteStartElement("map");
                                writer.WriteAttributeString("version", "1.0");
                                writer.WriteAttributeString("width", map.MapWidth.ToString(CultureInfo.InvariantCulture));
                                writer.WriteAttributeString("height", map.MapHeight.ToString(CultureInfo.InvariantCulture));

                                writer.WriteStartElement("entries");

                                Threading.QueueTask(x => this.OnProgress(0, "Processing" /*+ new string('.', dotCount)*/)).Wait();

                                var totalItems = map.MapWidth * map.MapHeight;

                                var idx = 0;
                                var idy = 0;
                                var completed = false;
                                //   var itemList = new Queue<ItemContainer>();
                                while (!this.isCanceled && !completed)
                                {
                                    Threading.QueueTask(
                                        x =>
                                        {
                                            var startTime = DateTime.Now;
                                            while (DateTime.Now < startTime + TimeSpan.FromSeconds(0.1))
                                            {
                                                var stack = map.Get(idx++, idy);
                                                if (stack == null)
                                                {
                                                    continue;
                                                }

                                                // itemList.Enqueue(new ItemContainer() { Location = new Point2D(idx, idy), Stack = stack });

                                                for (var i = 0; i < stack.Length; i++)
                                                {
                                                    if (this.isCanceled)
                                                    {
                                                        break;
                                                    }

                                                    //Threading.QueueTask(
                                                    //    x =>
                                                    //    this.OnProgress(
                                                    //        ((index / totalItems) * 100) + (((float)i / stack.Length) * 0.01f),
                                                    //        string.Format("Processing{2} {0}/{1}", Math.Round(index), totalItems, new string('.', dotCount)))).Wait();

                                                    var item = stack[i];
                                                    if (item == null)
                                                    {
                                                        continue;
                                                    }

                                                    var itemType = item.GetType();
                                                    var serializer = new XmlSerializer(itemType);
                                                    writer.WriteStartElement("entry");
                                                    writer.WriteAttributeString("x", idx.ToString(CultureInfo.InvariantCulture));
                                                    writer.WriteAttributeString("y", idy.ToString(CultureInfo.InvariantCulture));
                                                    writer.WriteAttributeString("layer", i.ToString(CultureInfo.InvariantCulture));
                                                    writer.WriteAttributeString("type", itemType.FullName);
                                                    serializer.Serialize(writer, item);
                                                    writer.WriteEndElement();
                                                }

                                                // idx++;
                                                if (idx > map.MapWidth - 1)
                                                {
                                                    idx = 0;
                                                    idy++;
                                                    if (idy > map.MapHeight - 1)
                                                    {
                                                        //       Threading.QueueTask(xx => Debug.Log("hereC"));
                                                        completed = true;
                                                        return;
                                                    }
                                                }

                                                var index = ((float)idy * map.MapWidth) + idx;
                                                // Threading.QueueTask(
                                                //  x =>
                                                this.OnProgress(
                                                    (index / totalItems) * 100,
                                                    string.Format(
                                                        "Processing{2} {0}/{1}",
                                                        Math.Round(index),
                                                        totalItems,
                                                        string.Empty/*new string('.', dotCount)*/)); //).Wait();

                                                //if (DateTime.Now > lastUpdate + TimeSpan.FromSeconds(0.5f))
                                                //{
                                                //    dotCount = (dotCount + 1) % 4;
                                                //    lastUpdate = DateTime.Now;
                                                //}

                                                if (this.isCanceled)
                                                {
                                                    return;
                                                }
                                            }
                                        }).Wait();
                                }

                                writer.WriteEndElement();
                                writer.WriteEndElement();
                            }
                        }

                        //  Threading.QueueTask(x => Debug.Log("hereY"));
                        if (!this.isCanceled)
                        {
                            if (File.Exists(path))
                            {
                                File.Delete(path);
                            }

                            File.Move(tempFile, path);
                        }

                        // Threading.QueueTask(x => Debug.Log("hereZ"));
                        //  this.isCanceled = false;
                        this.isProcessing = false;
                        this.isExporting = false;

                        Threading.QueueTask(
                            x =>
                            {
                                if (this.isCanceled)
                                {
                                    this.OnCanceledProgress();
                                }
                                else
                                {
                                    this.OnProgress(100, "Complete");
                                }
                            }).Wait();
                    }
                    catch (Exception ex)
                    {
                        Threading.QueueTask(x => this.OnProgress(ex));
                        throw ex;
                    }
                    finally
                    {
                        //  this.isCanceled = false;
                        this.isProcessing = false;
                        this.isExporting = false;
                    }
                },
            QueueType.BackgroundThread);
#endif
        }

        public string Title
        {
            get
            {
                return this.pluginInformation.Title;
            }
        }

        public string Extension
        {
            get
            {
                return ".xml";
            }
        }

        /// <summary>
        /// Occurs when exporter is processing.
        /// </summary>
        public event EventHandler<ProgressArgs> Progress;

        public void Cancel()
        {
            if (this.isExporting)
            {
                this.isCanceled = true;
                this.browseForFileScreen.ExitScreen();
            }
        }

        public void Export()
        {
            if (this.isExporting)
            {
                throw new ExporterException("Exporter is currently running.");
            }

            this.isCanceled = false;
            this.isExporting = true;
            this.isProcessing = false;
            this.editor.ScreenManager.AddScreen(this.browseForFileScreen, 0);
        }

        private void OnProgress()
        {
            this.args.State = ProgressState.Start;
            this.args.Message = string.Empty;
            var handler = this.Progress;
            if (handler != null)
            {
                handler(this, this.args);
            }
        }

        private void OnProgress(float progress, string message)
        {
            this.args.Progress = progress;
            this.args.Message = message;
            this.args.State = progress == 100f ? ProgressState.Completed : ProgressState.Processing;
            var handler = this.Progress;
            if (handler != null)
            {
                handler(this, this.args);
            }
        }

        private void OnProgress(Exception error)
        {
            this.args.Message = string.Empty;
            this.args.Error = error;
            var handler = this.Progress;
            if (handler != null)
            {
                handler(this, this.args);
            }
        }

        private void OnCanceledProgress()
        {
            this.args.Message = "Canceled";
            this.args.Error = null;
            this.args.State = ProgressState.Canceled;
            var handler = this.Progress;
            if (handler != null)
            {
                handler(this, this.args);
            }
        }
    }
}